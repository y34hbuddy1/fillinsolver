package fillinsolver

import (
	"bufio"
	"errors"
	"os"
)

func LoadWordsFromFile(filepath string, maxLength int) ([]string, error) {
	var ret []string

	file, err := os.Open(filepath)

	if err != nil {
		return ret, err
	}

	scanner := bufio.NewScanner(file)
	scanner.Split(bufio.ScanLines)

	for scanner.Scan() {
		wordlen := len(scanner.Text())

		if wordlen > maxLength {
			return ret, errors.New("word is too long")
		}

		for i := 0; i < wordlen; i++ {
			if !isCapitalLetterOrNumber(scanner.Text()[i]) {
				return ret, errors.New("invalid character found in word")
			}
		}

		ret = append(ret, scanner.Text())
	}

	_ = file.Close()

	return ret, nil
}

func RemoveWordFromList(word string, words []string) (newWords []string) {
	newWords = words

	for i := 0; i < len(newWords); i++ {
		if newWords[i] == word {
			copy(newWords[i:], newWords[i+1:])    // Shift a[i+1:] left one index.
			newWords[len(newWords)-1] = ""        // Erase last element (write zero value).
			newWords = newWords[:len(newWords)-1] // Truncate slice.
			break
		}
	}

	return newWords
}

func GetWordsWithLength(words []string, length int) (matchingWords []string) {
	for i := 0; i < len(words); i++ {
		if len(words[i]) == length {
			matchingWords = append(matchingWords, words[i])
		}
	}

	return matchingWords
}

// Input:  an array of words
// Output: a subset of Input with only the words of unique length (if any)
func GetWordsOfUniqueLength(words []string, b Board) []string {
	var ret []string

	wordsLen := len(words)

	var maxWordLen int

	if b.ROWS > b.COLS || b.ROWS == b.COLS {
		maxWordLen = b.ROWS
	} else {
		maxWordLen = b.COLS
	}

	lengthCounter := make([]int, maxWordLen) // keeps track of how many words are of each length
	lengthTracker := make([]int, wordsLen)   // keeps track of the length of each word

	for i := 0; i < wordsLen; i++ {
		curLen := len(words[i])

		lengthCounter[curLen]++
		lengthTracker[i] = curLen
	}

	// are there any lengths for which there is only one word?
	for i := 0; i < maxWordLen; i++ {
		if lengthCounter[i] == 1 {
			// find the word of this length and append it to our result
			for j := 0; j < wordsLen; j++ {
				if lengthTracker[j] == i {
					ret = append(ret, words[j])
					break
				}
			}
		}
	}

	return ret
}

/* This is a useful function but not currently used
func IsWordInList(word string, words []string) bool {
	for i := 0; i < len(words); i++ {
		if words[i] == word {
			return true
		}
	}

	return false
}
*/

/* This is a useful function but not currently used
func AreWordsInList(testWords []string, words []string) bool {
	for i := 0; i < len(testWords); i++ {
		if !IsWordInList(testWords[i], words) {
			return false
		}
	}

	return true
}
*/

/* This is a useful function but not currently used
func GetWordsOfGivenLengthWithLettersAtIndices(words []string, length int, indices []int, letters []byte) (matchingWords []string) {
	if len(indices) != len(letters) {
		return matchingWords
	}

	wordsOfLength := GetWordsWithLength(words, length)

	for i := 0; i < len(wordsOfLength); i++ {
		for j := 0; j < len(indices); j++ {
			if wordsOfLength[i][indices[j]] != letters[j] {
				break
			}

			if j == len(indices)-1 {
				matchingWords = append(matchingWords, wordsOfLength[i])
			}
		}
	}

	return matchingWords
}
*/
