package main

import (
	"errors"
	"fmt"
	"os"
	"quint.as/fillinsolver"
	"strconv"
)

func solveBoard(board fillinsolver.Board, words []string) (solvedBoard fillinsolver.Board, leftoverWords []string, err error) {
	// make a copy of our board to compare for changes later
	var origBoard fillinsolver.Board
	origBoard.CopyFrom(board)

	// check for any already-solved words
	words = board.RemoveSolvedWords(words)

	// check for completion
	if board.IsFull() {
		if len(words) == 0 {
			return board, words, nil /* success! */
		} else {
			return board, words, errors.New("board is full but there are still " + strconv.Itoa(len(words)) + " words remaining")
		}
	} else {
		if len(words) == 0 {
			return board, words, errors.New("board is not full but there are no words remaining")
		}
	}

	// algorithm step 1: check if there are any words that are the only one of its length
	err = doUniqueLengthSearch(&board, &words)

	if err != nil {
		return board, words, err
	}

	// check to see if any other words were solved by placing the above words
	words = board.RemoveSolvedWords(words)

	// algorithm step 2: check if there are any words that uniquely fit based on letters already on the board
	err = doUniqueFitSearch(&board, &words)

	if err != nil {
		return board, words, err
	}

	// check to see if any other words were solved by placing the above words
	words = board.RemoveSolvedWords(words)

	// algorithm step 3: check if there are any words that uniquely fit based on other words that intersect it
	err = doAdvancedSearch(&board, &words)

	if err != nil {
		return board, words, err
	}

	// check to see if any other words were solved by placing the above words
	words = board.RemoveSolvedWords(words)

	// if this pass didn't add any words, then we failed
	if board.Equals(origBoard) {
		return board, words, errors.New("iterated without any changes")
	}

	// repeat the above algorithm (recurse) until we solve the board
	board, words, err = solveBoard(board, words)

	// check to see if any other words were solved by placing the above words
	//noinspection GoNilness
	words = board.RemoveSolvedWords(words)

	if err != nil {
		return board, words, errors.New("failed to solve board: " + err.Error())
	}

	return board, words, nil
}

func doUniqueLengthSearch(board *fillinsolver.Board, words *[]string) error {
	wordsOfUniqueLength := fillinsolver.GetWordsOfUniqueLength(*words, *board)

	for len(wordsOfUniqueLength) > 0 {
		for i := len(wordsOfUniqueLength) - 1; i >= 0; i-- {
			// find out where this word of unique length fits on the board
			curWord := wordsOfUniqueLength[i]

			rowH, colH, err := board.FindFirstAvailableSpotHorizontal(curWord)
			if err != nil {
				rowV, colV, err := board.FindFirstAvailableSpotVertical(curWord)

				if err != nil {
					return errors.New("word " + curWord + " is supposed to be of unique length, but there is no available spot for it: " + err.Error())
				}

				err = board.PlaceWordVertical(rowV, colV, curWord)

				if err != nil {
					return errors.New("failed to place word " + curWord + " vertically: " + err.Error())
				}

				*words = fillinsolver.RemoveWordFromList(curWord, *words)
			} else {
				err = board.PlaceWordHorizontal(rowH, colH, curWord)

				if err != nil {
					return errors.New("failed to place word " + curWord + " horizontally: " + err.Error())
				}

				*words = fillinsolver.RemoveWordFromList(curWord, *words)
			}
		}

		wordsOfUniqueLength = fillinsolver.GetWordsOfUniqueLength(*words, *board)
	}

	return nil
}

func doUniqueFitSearch(board *fillinsolver.Board, words *[]string) error {
	uniqueFits, err := findWordsThatUniquelyFit(*board, *words)

	if err != nil {
		return errors.New("failed to find words that uniquely fit")
	}

	for len(uniqueFits) > 0 {
		for i := 0; i < len(uniqueFits); i++ {
			// remove word from words list
			*words = fillinsolver.RemoveWordFromList(uniqueFits[i].Word, *words)

			if uniqueFits[i].Direction == fillinsolver.HORIZONTAL {
				err = board.PlaceWordHorizontal(uniqueFits[i].Coords[0], uniqueFits[i].Coords[1], uniqueFits[i].Word)
				if err != nil {
					return errors.New("failed to place word horizontally: " + err.Error())
				}
			} else if uniqueFits[i].Direction == fillinsolver.VERTICAL {
				err = board.PlaceWordVertical(uniqueFits[i].Coords[0], uniqueFits[i].Coords[1], uniqueFits[i].Word)
				if err != nil {
					return errors.New("failed to place word vertically: " + err.Error())
				}
			} else {
				return errors.New("findWordsThatUniquelyFit() returned a result with an invalid direction")
			}
		}

		uniqueFits, err = findWordsThatUniquelyFit(*board, *words)

		if err != nil {
			return errors.New("failed to find words that uniquely fit")
		}
	}

	return nil
}

func findPlacementsThatAreNotCompletelyEmpty(board fillinsolver.Board, words []string) ([]fillinsolver.Placement, error) {
	var possibilities []fillinsolver.Placement

	for i := 0; i < board.ROWS; i++ {
		for j := 0; j < board.COLS; j++ {
			if board.IsStartOfWordHorizontal(i, j) {
				curWordLen := board.LengthOfPossibleWordHorizontal(i, j)
				wordsToCheck := fillinsolver.GetWordsWithLength(words, curWordLen)
				nonEmptySpots, _ := board.FindNonEmptySpotsInWord([2]int{i, j}, fillinsolver.HORIZONTAL)
				if len(nonEmptySpots) != curWordLen {
					nonEmptySpotsRel := translateNonEmptyToRelative(nonEmptySpots, [2]int{i, j}, fillinsolver.HORIZONTAL)
					if len(nonEmptySpotsRel) != len(nonEmptySpots) {
						return nil, errors.New("length of nonEmptySpotsRel != length of nonEmptySpots")
					}
					for k := 0; k < len(wordsToCheck); k++ {
						for l := 0; l < len(nonEmptySpotsRel); l++ {
							if wordsToCheck[k][nonEmptySpotsRel[l]] != board.Coords[nonEmptySpots[l][0]][nonEmptySpots[l][1]] {
								break
							}

							if l == len(nonEmptySpotsRel)-1 {
								possibilities = append(possibilities, fillinsolver.Placement{
									Word:      wordsToCheck[k],
									Coords:    [2]int{i, j},
									Direction: fillinsolver.HORIZONTAL,
								})
							}
						}
					}
				}
			}

			if board.IsStartOfWordVertical(i, j) {
				curWordLen := board.LengthOfPossibleWordVertical(i, j)
				wordsToCheck := fillinsolver.GetWordsWithLength(words, curWordLen)
				nonEmptySpots, _ := board.FindNonEmptySpotsInWord([2]int{i, j}, fillinsolver.VERTICAL)
				if len(nonEmptySpots) != curWordLen {
					nonEmptySpotsRel := translateNonEmptyToRelative(nonEmptySpots, [2]int{i, j}, fillinsolver.VERTICAL)
					if len(nonEmptySpotsRel) != len(nonEmptySpots) {
						return nil, errors.New("length of nonEmptySpotsRel != length of nonEmptySpots")
					}
					for k := 0; k < len(wordsToCheck); k++ {
						for l := 0; l < len(nonEmptySpotsRel); l++ {
							if wordsToCheck[k][nonEmptySpotsRel[l]] != board.Coords[nonEmptySpots[l][0]][nonEmptySpots[l][1]] {
								break
							}

							if l == len(nonEmptySpotsRel)-1 {
								possibilities = append(possibilities, fillinsolver.Placement{
									Word:      wordsToCheck[k],
									Coords:    [2]int{i, j},
									Direction: fillinsolver.VERTICAL,
								})
							}
						}
					}
				}
			}
		}
	}

	return possibilities, nil
}

func findWordsThatUniquelyFit(board fillinsolver.Board, words []string) ([]fillinsolver.Placement, error) {
	var ret []fillinsolver.Placement

	possibilities, err := findPlacementsThatAreNotCompletelyEmpty(board, words)

	if err != nil {
		return nil, errors.New("failed to find placements that are not completely empty: " + err.Error())
	}

	// check all possibilities for uniqueness
	if possibilities != nil {
		possibilitiesLen := len(possibilities)
		for i := 0; i < possibilitiesLen; i++ {
			curPossibility := possibilities[i]
			for j := 0; j < possibilitiesLen; j++ {
				if j == i {
					if j == possibilitiesLen-1 {
						// this is a unique possibility
						ret = append(ret, curPossibility)
					} else {
						continue
					}
				}

				if curPossibility.Coords[0] == possibilities[j].Coords[0] &&
					curPossibility.Coords[1] == possibilities[j].Coords[1] &&
					curPossibility.Direction == possibilities[j].Direction {
					// this is not a unique possibility
					break
				}

				if j == possibilitiesLen-1 {
					// this is a unique possibility
					ret = append(ret, curPossibility)
				}
			}
		}
	}

	return ret, nil
}

func doAdvancedSearch(board *fillinsolver.Board, words *[]string) error {
	for i := 0; i < board.ROWS; i++ {
		for j := 0; j < board.COLS; j++ {
			if !board.IsOpenSpot(i, j) && !board.IsBlackSpot(i, j) {
				err := doAdvancedSearchForSpot(board, words, i, j)
				if err != nil {
					return err
				}
			}
		}
	}

	return nil
}

func doAdvancedSearchForSpot(board *fillinsolver.Board, words *[]string, row int, col int) error {
	err := doAdvancedSearchForSpotByDirection(board, words, row, col, fillinsolver.VERTICAL)
	if err != nil {
		return err
	}

	err = doAdvancedSearchForSpotByDirection(board, words, row, col, fillinsolver.HORIZONTAL)
	if err != nil {
		return err
	}

	return nil
}

func doAdvancedSearchForSpotByDirection(board *fillinsolver.Board, words *[]string, row int, col int, direction int) error {
	refinedPossibleWords, startRow, startCol, err := getSecondDegreePossibilities(*board, *words, row, col, direction)

	if err != nil {
		return errors.New("failed to get second degree possibilities: " + err.Error())
	}

	// are we left with only 1 refined possible word? if so, place it and remove from words list. If not, skip it and move on.
	if len(refinedPossibleWords) == 1 {
		if direction == fillinsolver.VERTICAL {
			err := board.PlaceWordVertical(startRow, col, refinedPossibleWords[0])
			if err != nil {
				return err
			}
		} else {
			err := board.PlaceWordHorizontal(row, startCol, refinedPossibleWords[0])
			if err != nil {
				return err
			}
		}

		*words = fillinsolver.RemoveWordFromList(refinedPossibleWords[0], *words)
	} else if len(refinedPossibleWords) > 1 {
		// try placing each word and doing a brute force test
		for i := 0; i < len(refinedPossibleWords); i++ {
			var tmpBoard fillinsolver.Board
			tmpWords := make([]string, len(*words))

			//noinspection GoNilness
			tmpBoard.CopyFrom(*board)
			copy(tmpWords, *words)

			if direction == fillinsolver.VERTICAL {
				//noinspection GoNilness
				err := tmpBoard.PlaceWordVertical(startRow, col, refinedPossibleWords[i])
				if err != nil {
					return err
				}
			} else {
				//noinspection GoNilness
				err := tmpBoard.PlaceWordHorizontal(row, startCol, refinedPossibleWords[i])
				if err != nil {
					return err
				}
			}

			tmpBoard, tmpWords, err = solveBoard(tmpBoard, tmpWords)

			if err != nil || tmpBoard.Equals(*board) {
				continue
			}

			// brute force worked! let's pass our solved board back down the stack.
			board.CopyFrom(tmpBoard)
			*words = tmpWords
			return nil
		}
	}

	return nil
}

// for a given spot and direction, find any words that are validated to fit not only by existing letters, but also
// by making sure that, for each letter in the word, there are possible words going in the other direction
func getSecondDegreePossibilities(board fillinsolver.Board, words []string, row int, col int, direction int) (refinedPossibleWords []string, startRow int, startCol int, err error) {
	var possibleWords []string

	if direction == fillinsolver.VERTICAL {
		startRow = board.GetVerticalWordStart(row, col)
		possibleWords = board.GetWordsThatFitVertical(startRow, col, words)
	} else if direction == fillinsolver.HORIZONTAL {
		startCol = board.GetHorizontalWordStart(row, col)
		possibleWords = board.GetWordsThatFitHorizontal(row, startCol, words)
	} else {
		return nil, -1, -1, errors.New("invalid direction provided to getSecondDegreePossibilities()")
	}

	// for each possible word, check each letter to rule out words based on adjacent letters
	for i := 0; i < len(possibleWords); i++ {
		// pretend we placed this word here, and for each letter in the word, verify that there are
		// possible words going in the other direction
		var tmpBoard fillinsolver.Board
		tmpBoard.CopyFrom(board)

		if direction == fillinsolver.VERTICAL {
			err := tmpBoard.PlaceWordVertical(startRow, col, possibleWords[i])
			if err != nil {
				return nil, -1, -1, err
			}
		} else {
			err := tmpBoard.PlaceWordHorizontal(row, startCol, possibleWords[i])
			if err != nil {
				return nil, -1, -1, err
			}
		}

		for j := 0; j < len(possibleWords[i]); j++ {
			if direction == fillinsolver.VERTICAL {
				tmpHStart := tmpBoard.GetHorizontalWordStart(startRow+j, col)
				tmpHWords := tmpBoard.GetWordsThatFitHorizontal(startRow+j, tmpHStart, words)

				if len(tmpHWords) == 0 && !tmpBoard.IsWordFilledHorizontal(startRow+j, tmpHStart) {
					break
				}
			} else {
				tmpVStart := tmpBoard.GetVerticalWordStart(row, startCol+j)
				tmpVWords := tmpBoard.GetWordsThatFitVertical(tmpVStart, startCol+j, words)

				if len(tmpVWords) == 0 && !tmpBoard.IsWordFilledVertical(tmpVStart, startCol+j) {
					break
				}
			}

			if j == (len(possibleWords[i]) - 1) {
				refinedPossibleWords = append(refinedPossibleWords, possibleWords[i])
			}
		}
	}

	return refinedPossibleWords, startRow, startCol, nil
}

func translateNonEmptyToRelative(nonEmptySpots [][2]int, wordStart [2]int, direction int) []int {
	var ret []int

	for i := 0; i < len(nonEmptySpots); i++ {
		if direction == fillinsolver.HORIZONTAL {
			ret = append(ret, nonEmptySpots[i][1]-wordStart[1])
		} else if direction == fillinsolver.VERTICAL {
			ret = append(ret, nonEmptySpots[i][0]-wordStart[0])
		}
	}

	return ret
}

func main() {
	if len(os.Args) < 3 {
		fmt.Println("usage: " + os.Args[0] + " [board file] [words file]")
		os.Exit(1)
	}

	boardFilepath := os.Args[1]
	wordsFilepath := os.Args[2]

	board, err := fillinsolver.NewBoardFromFile(boardFilepath)
	if err != nil {
		fmt.Println("Error: failed to load board at path " + boardFilepath + ": " + err.Error())
		os.Exit(1)
	}

	words, err := fillinsolver.LoadWordsFromFile(wordsFilepath, board.MAXWORDLEN())
	if err != nil {
		fmt.Println("Error: failed to load words at path " + wordsFilepath + ": " + err.Error())
		os.Exit(1)
	}

	wordslen := len(words)

	if wordslen == 0 {
		fmt.Println("Error: no words were loaded")
		os.Exit(1)
	}

	board.Print()
	fmt.Println("")

	solvedBoard, words, err := solveBoard(board, words)

	if err != nil {
		fmt.Println("Error: failed to solve board: " + err.Error())
		fmt.Println("")
		//noinspection GoNilness
		solvedBoard.Print()
		fmt.Println("LEFTOVER WORDS:")
		for i := 0; i < len(words); i++ {
			fmt.Println("  " + words[i] + "(" + strconv.Itoa(len(words[i])) + ")")
		}
		os.Exit(1)
	} else {
		solvedBoard.Print()
	}
}
