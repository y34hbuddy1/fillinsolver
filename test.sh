#!/bin/sh

cd cmd/fillinsolver
go build fillinsolver.go
cd ../..

for boardnum in 002 003 004 005 006 007 008 009 010 011 012 013 014 015 016 017 018
do
  printf "testing board #$boardnum..."
  echo "BOARD #$boardnum" >> fillinsolver.log
  cmd/fillinsolver/fillinsolver "test_boards/board$boardnum.txt" "test_boards/board$boardnum""_words.txt" >> fillinsolver.log
  retVal=$?
  echo "" >> fillinsolver.log
  if [ $retVal -ne 0 ]; then
    printf "FAILED!\n"
  else
    printf "SUCCEEDED!\n"
  fi
done
