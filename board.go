package fillinsolver

import (
	"errors"
	"fmt"
	"io/ioutil"
	"math"
	"strconv"
)

const BLACK = byte('=')
const OPEN = byte(' ')

const HORIZONTAL = 0
const VERTICAL = 1

type Board struct {
	ROWS   int
	COLS   int
	Coords [][]byte
}

func (b Board) MAXBOARDSTR() int {
	return (2*b.COLS)*b.ROWS - 1
}

func (b Board) MAXWORDLEN() int {
	if b.ROWS >= b.COLS {
		return b.ROWS
	} else {
		return b.COLS
	}
}

func (b Board) Print() {
	for i := 0; i < b.ROWS; i++ {
		for j := 0; j < b.COLS; j++ {
			if b.IsBlackSpot(i, j) {
				fmt.Printf("= ")
			} else if b.IsOpenSpot(i, j) {
				fmt.Printf("  ")
			} else {
				fmt.Printf(string(b.Coords[i][j]) + " ")
			}
		}
		fmt.Printf("\n")
	}
}

func (b Board) Equals(board Board) bool {
	if b.ROWS != board.ROWS || b.COLS != board.COLS {
		return false
	}

	for i := 0; i < b.ROWS; i++ {
		for j := 0; j < b.COLS; j++ {
			if b.Coords[i][j] != board.Coords[i][j] {
				return false
			}
		}
	}

	return true
}

func (b Board) CopyTo(board *Board) {
	board.ROWS = b.ROWS
	board.COLS = b.COLS
	board.Coords = make([][]byte, b.ROWS)

	for i := 0; i < b.ROWS; i++ {
		board.Coords[i] = make([]byte, b.COLS)

		for j := 0; j < b.COLS; j++ {
			board.Coords[i][j] = b.Coords[i][j]
		}
	}
}

func (b *Board) CopyFrom(board Board) {
	b.ROWS = board.ROWS
	b.COLS = board.COLS
	b.Coords = make([][]byte, board.ROWS)

	for i := 0; i < b.ROWS; i++ {
		b.Coords[i] = make([]byte, board.COLS)

		for j := 0; j < b.COLS; j++ {
			b.Coords[i][j] = board.Coords[i][j]
		}
	}
}

func NewBoardFromFile(filepath string) (Board, error) {
	var retBoard Board

	dat, err := ioutil.ReadFile(filepath)

	if err != nil {
		return retBoard, err
	}

	rows, cols := getBoardDimensionsFromSource(string(dat))
	retBoard.ROWS = rows
	retBoard.COLS = cols
	retBoard.Coords = make([][]byte, rows)
	for i := 0; i < retBoard.ROWS; i++ {
		for j := 0; j < retBoard.COLS; j++ {
			retBoard.Coords[i] = make([]byte, cols)
		}
	}

	err = validateBoardString(string(dat), retBoard)

	if err != nil {
		return retBoard, err
	}

	col := 0

	for i := 0; i < retBoard.MAXBOARDSTR(); i += 2 {
		row := math.Floor(float64(i / (2 * retBoard.COLS)))

		retBoard.Coords[int(row)][col] = string(dat)[i]

		col++

		if col == retBoard.COLS {
			col = 0
		}

	}

	return retBoard, nil
}

func (b *Board) LoadFromFile(filepath string) error {
	ret, err := NewBoardFromFile(filepath)

	if err != nil {
		return err
	}

	*b = ret

	return nil
}

func (b Board) IsFull() bool {
	for i := 0; i < b.ROWS; i++ {
		for j := 0; j < b.COLS; j++ {
			if b.IsOpenSpot(i, j) {
				return false
			}
		}
	}

	return true
}

func (b Board) IsWordOnBoard(word string) bool {
	for i := 0; i < b.ROWS; i++ {
		for j := 0; j < b.COLS; j++ {
			if b.IsStartOfWordHorizontal(i, j) && b.LengthOfPossibleWordHorizontal(i, j) == len(word) {
				for k := 0; k < len(word); k++ {
					if b.Coords[i][j+k] != word[k] {
						break
					}

					if k == len(word)-1 {
						return true
					}
				}
			}

			if b.IsStartOfWordVertical(i, j) && b.LengthOfPossibleWordVertical(i, j) == len(word) {
				for k := 0; k < len(word); k++ {
					if b.Coords[i+k][j] != word[k] {
						break
					}

					if k == len(word)-1 {
						return true
					}
				}
			}
		}
	}

	return false
}

func getBoardDimensionsFromSource(boardStr string) (rows int, cols int) {
	rows = 1

	gotCols := false

	for i := 0; i < len(boardStr); i++ {
		if boardStr[i] == '\n' {
			rows++
			if !gotCols {
				cols = (i + 1) / 2
				gotCols = true
			}
		}
	}

	return rows, cols
}

func validateBoardString(boardStr string, b Board) error {
	// check total board size
	if len(boardStr) != b.MAXBOARDSTR() {
		return errors.New("invalid board length")
	}

	// check number of rows
	newlinecount := 0

	for i := 0; i < b.MAXBOARDSTR(); i++ {
		if boardStr[i] == '\n' {
			newlinecount++
		}
	}

	if newlinecount != (b.ROWS - 1) {
		return errors.New("invalid number of rows")
	}

	// check each row for validity
	for i := 0; i < b.ROWS; i++ {
		row := boardStr[(i * 2 * b.COLS) : (i*2*b.COLS)+(2*b.COLS-1)]

		for j := 0; j < (2 * b.COLS); j += 2 {
			if !isCapitalLetterOrNumber(row[j]) && !isSpecialBoardCharacter(row[j]) {
				return errors.New("invalid character at row " + strconv.Itoa(i+1) + ", column " + strconv.Itoa(j+1))
			}

			if j < (2*b.COLS - 2) {
				if !isSpace(row[j+1]) {
					return errors.New("missing a [space] at row " + strconv.Itoa(i+1) + ", column " + strconv.Itoa(j+2))
				}
			}
		}
	}

	return nil
}

func isSpecialBoardCharacter(c byte) bool {
	if c == BLACK || c == OPEN {
		return true
	}

	return false
}

func isCapitalLetterOrNumber(c byte) bool {
	if (c >= '0' && c <= '9') || (c >= 'A' && c <= 'Z') {
		return true
	}

	return false
}

func isSpace(c byte) bool {
	if c == ' ' {
		return true
	}

	return false
}
