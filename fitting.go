package fillinsolver

import (
	"errors"
	"strconv"
)

type Placement struct {
	Word      string
	Coords    [2]int
	Direction int
}

func (b Board) IsStartOfWordHorizontal(row int, col int) bool {
	if b.IsBlackSpot(row, col) {
		return false
	}

	if col == 0 || b.IsBlackSpot(row, col-1) {
		return true
	}

	return false
}

func (b Board) IsStartOfWordVertical(row int, col int) bool {
	if b.IsBlackSpot(row, col) {
		return false
	}

	if row == 0 || b.IsBlackSpot(row-1, col) {
		return true
	}

	return false
}

func (b Board) GetHorizontalWordStart(row int, col int) (startCol int) {
	// move left until we find a spot for which IsStartOfWordHorizontal = true
	if col == 0 || b.IsStartOfWordHorizontal(row, col) {
		return col
	}

	for i := 1; i <= col; i++ {
		if b.IsStartOfWordHorizontal(row, col-i) {
			return col - i
		}
	}

	return -1 // this should never happen
}

func (b Board) GetVerticalWordStart(row int, col int) (startRow int) {
	// move up until we find a spot for which IsStartOfWordVertical = true
	if row == 0 || b.IsStartOfWordVertical(row, col) {
		return row
	}

	for i := 1; i <= row; i++ {
		if b.IsStartOfWordVertical(row-i, col) {
			return row - i
		}
	}

	return -1 // this should never happen
}

func (b Board) LengthOfPossibleWordHorizontal(row int, col int) int {
	if !b.IsStartOfWordHorizontal(row, col) {
		return 0
	}

	availCount := 0

	for i := 0; (col + i) < b.COLS; i++ {
		if b.IsBlackSpot(row, col+i) {
			break
		}

		availCount++
	}

	return availCount
}

func (b Board) LengthOfPossibleWordVertical(row int, col int) int {
	if !b.IsStartOfWordVertical(row, col) {
		return 0
	}

	availCount := 0

	for i := 0; (row + i) < b.ROWS; i++ {
		if b.IsBlackSpot(row+i, col) {
			break
		}

		availCount++
	}

	return availCount
}

func (b Board) IsOpenSpot(row int, col int) bool {
	if b.Coords[row][col] == OPEN {
		return true
	}

	return false
}

func (b Board) IsBlackSpot(row int, col int) bool {
	if b.Coords[row][col] == BLACK {
		return true
	}

	return false
}

func (b Board) DoesWordFitHorizontal(row int, col int, word string) bool {
	// if this isn't where a word should begin, return false
	if !b.IsStartOfWordHorizontal(row, col) {
		return false
	}

	wordlen := len(word)

	// if word isn't same size as available space, return false
	if b.LengthOfPossibleWordHorizontal(row, col) != len(word) {
		return false
	}

	for i := 0; i < wordlen; i++ {
		// if the spot isn't open, check to see if it's filled with a letter
		// that matches our word
		if !b.IsOpenSpot(row, col+i) && b.Coords[row][col+i] != word[i] {
			return false
		}
	}

	return true
}

func (b Board) DoesWordFitVertical(row int, col int, word string) bool {
	// if this isn't where a word should begin, return false
	if !b.IsStartOfWordVertical(row, col) {
		return false
	}

	wordlen := len(word)

	// if word isn't same size as available space, return false
	if b.LengthOfPossibleWordVertical(row, col) != len(word) {
		return false
	}

	for i := 0; i < wordlen; i++ {
		// if the spot isn't open, check to see if it's filled with a letter
		// that matches our word
		if !b.IsOpenSpot(row+i, col) && b.Coords[row+i][col] != word[i] {
			return false
		}
	}

	return true
}

func (b Board) IsWordFilledHorizontal(row int, col int) bool {
	if !b.IsStartOfWordHorizontal(row, col) {
		return false
	}

	length := b.LengthOfPossibleWordHorizontal(row, col)

	for i := 0; i < length; i++ {
		if b.IsOpenSpot(row, col+i) {
			return false
		}
	}

	return true
}

func (b Board) IsWordFilledVertical(row int, col int) bool {
	if !b.IsStartOfWordVertical(row, col) {
		return false
	}

	length := b.LengthOfPossibleWordVertical(row, col)

	for i := 0; i < length; i++ {
		if b.IsOpenSpot(row+i, col) {
			return false
		}
	}

	return true
}

func (b Board) GetFilledWordHorizontal(row int, col int) string {
	var ret []byte

	if !b.IsWordFilledHorizontal(row, col) {
		return ""
	}

	length := b.LengthOfPossibleWordHorizontal(row, col)

	for i := 0; i < length; i++ {
		ret = append(ret, b.Coords[row][col+i])
	}

	return string(ret)
}

func (b Board) GetFilledWordVertical(row int, col int) string {
	var ret []byte

	if !b.IsWordFilledVertical(row, col) {
		return ""
	}

	length := b.LengthOfPossibleWordVertical(row, col)

	for i := 0; i < length; i++ {
		ret = append(ret, b.Coords[row+i][col])
	}

	return string(ret)
}

func (b Board) GetAllFilledWords() []string {
	var ret []string

	for i := 0; i < b.ROWS; i++ {
		for j := 0; j < b.COLS; j++ {
			if b.IsWordFilledHorizontal(i, j) {
				ret = append(ret, b.GetFilledWordHorizontal(i, j))
			}

			if b.IsWordFilledVertical(i, j) {
				ret = append(ret, b.GetFilledWordVertical(i, j))
			}
		}
	}

	return ret
}

// this function isn't looking at any adjacent words/letters. It purely checks for possible words
// based on the length and letters we already know
func (b Board) GetWordsThatFitHorizontal(row int, col int, words []string) []string {
	var ret []string

	if !b.IsStartOfWordHorizontal(row, col) {
		return ret
	}

	if b.IsWordFilledHorizontal(row, col) {
		return ret
	}

	length := b.LengthOfPossibleWordHorizontal(row, col)

	// grab possible words
	possibleWords := GetWordsWithLength(words, length)
	nonEmptySpots, err := b.FindNonEmptySpotsInWord([2]int{row, col}, HORIZONTAL)

	if err != nil {
		return ret
	}

	// refine possible words based on letters we already have on board
	var nonEmptySpotsRel []int
	for i := 0; i < len(nonEmptySpots); i++ {
		nonEmptySpotsRel = append(nonEmptySpotsRel, nonEmptySpots[i][1]-col)
	}

	for i := 0; i < len(possibleWords); i++ {
		for j := 0; j < len(nonEmptySpotsRel); j++ {
			if possibleWords[i][nonEmptySpotsRel[j]] != b.Coords[row][col+nonEmptySpotsRel[j]] {
				break
			}

			if j == len(nonEmptySpotsRel)-1 {
				ret = append(ret, possibleWords[i])
			}
		}
	}

	return ret
}

// this function isn't looking at any adjacent words/letters. It purely checks for possible words
// based on the length and letters we already know
func (b Board) GetWordsThatFitVertical(row int, col int, words []string) []string {
	var ret []string

	if !b.IsStartOfWordVertical(row, col) {
		return ret
	}

	if b.IsWordFilledVertical(row, col) {
		return ret
	}

	length := b.LengthOfPossibleWordVertical(row, col)

	// grab possible words
	possibleWords := GetWordsWithLength(words, length)
	nonEmptySpots, err := b.FindNonEmptySpotsInWord([2]int{row, col}, VERTICAL)

	if err != nil {
		return ret
	}

	// refine possible words based on letters we already have on board
	var nonEmptySpotsRel []int
	for i := 0; i < len(nonEmptySpots); i++ {
		nonEmptySpotsRel = append(nonEmptySpotsRel, nonEmptySpots[i][0]-row)
	}

	for i := 0; i < len(possibleWords); i++ {
		for j := 0; j < len(nonEmptySpotsRel); j++ {
			if possibleWords[i][nonEmptySpotsRel[j]] != b.Coords[row+nonEmptySpotsRel[j]][col] {
				break
			}

			if j == len(nonEmptySpotsRel)-1 {
				ret = append(ret, possibleWords[i])
			}
		}
	}

	return ret
}

func (b Board) FindNonEmptySpotsInWord(start [2]int, direction int) (nonEmptySpots [][2]int, err error) {
	if direction == HORIZONTAL {
		curLen := b.LengthOfPossibleWordHorizontal(start[0], start[1])
		for i := 0; i < curLen; i++ {
			if !b.IsOpenSpot(start[0], start[1]+i) {
				nonEmptySpots = append(nonEmptySpots, [2]int{start[0], start[1] + i})
			}
		}
	} else if direction == VERTICAL {
		curLen := b.LengthOfPossibleWordVertical(start[0], start[1])
		for i := 0; i < curLen; i++ {
			if !b.IsOpenSpot(start[0]+i, start[1]) {
				nonEmptySpots = append(nonEmptySpots, [2]int{start[0] + i, start[1]})
			}
		}
	} else {
		return nonEmptySpots, errors.New("invalid direction: " + strconv.Itoa(direction))
	}

	return nonEmptySpots, nil
}

func (b *Board) FindFirstAvailableSpotHorizontal(word string) (row int, col int, err error) {
	for i := 0; i < b.ROWS; i++ {
		for j := 0; j < b.COLS; j++ {
			if b.DoesWordFitHorizontal(i, j, word) {
				return i, j, nil
			}
		}
	}

	return -1, -1, errors.New("no available horizontal spot for word " + word)
}

func (b *Board) FindFirstAvailableSpotVertical(word string) (row int, col int, err error) {
	for i := 0; i < b.ROWS; i++ {
		for j := 0; j < b.COLS; j++ {
			if b.DoesWordFitVertical(i, j, word) {
				return i, j, nil
			}
		}
	}

	return -1, -1, errors.New("no available vertical spot for word " + word)
}

func (b *Board) FindAllAvailableSpotsHorizontal(word string) (spots [][2]int, err error) {
	ret := make([][2]int, 1)

	faRow, faCol, err := b.FindFirstAvailableSpotHorizontal(word)
	if err != nil {
		return spots, errors.New("no available horizontal spots for word " + word)
	}

	ret[0][0] = faRow
	ret[0][1] = faCol

	/* block the spot so that it isn't considered available for next iteration */
	if b.IsOpenSpot(faRow, faCol) {
		b.Coords[faRow][faCol] = '?'
	} else if b.Coords[faRow][faCol] >= 'A' && b.Coords[faRow][faCol] <= 'Z' {
		b.Coords[faRow][faCol] = b.Coords[faRow][faCol] + 'a' - 'A'
	} else if b.Coords[faRow][faCol] >= '0' && b.Coords[faRow][faCol] <= '9' {
		b.Coords[faRow][faCol] = b.Coords[faRow][faCol] - 10
	}

	for true {
		faRow, faCol, err := b.FindFirstAvailableSpotHorizontal(word)

		if err != nil {
			break
		}

		var faCoords [2]int
		faCoords[0] = faRow
		faCoords[1] = faCol

		ret = append(ret, faCoords)

		/* block the spot so that it isn't considered available for next iteration */
		if b.IsOpenSpot(faRow, faCol) {
			b.Coords[faRow][faCol] = '?'
		} else if b.Coords[faRow][faCol] >= 'A' && b.Coords[faRow][faCol] <= 'Z' {
			b.Coords[faRow][faCol] = b.Coords[faRow][faCol] + 'a' - 'A'
		} else if b.Coords[faRow][faCol] >= '0' && b.Coords[faRow][faCol] <= '9' {
			b.Coords[faRow][faCol] = b.Coords[faRow][faCol] - 10
		}
	}

	/* remove all placeholders */
	for i := 0; i < b.ROWS; i++ {
		for j := 0; j < b.COLS; j++ {
			if b.Coords[i][j] == '?' {
				b.Coords[i][j] = OPEN
			} else if b.Coords[i][j] >= 'a' && b.Coords[i][j] <= 'z' {
				b.Coords[i][j] = b.Coords[i][j] + 'A' - 'a'
			} else if b.Coords[i][j] >= 38 && b.Coords[i][j] <= 47 {
				b.Coords[i][j] = b.Coords[i][j] + 10
			}
		}
	}

	return ret, nil
}

func (b *Board) FindAllAvailableSpotsVertical(word string) (spots [][2]int, err error) {
	ret := make([][2]int, 1)

	faRow, faCol, err := b.FindFirstAvailableSpotVertical(word)
	if err != nil {
		return spots, errors.New("no available vertical spots for word " + word)
	}

	ret[0][0] = faRow
	ret[0][1] = faCol

	/* block the spot so that it isn't considered available for next iteration */
	if b.IsOpenSpot(faRow, faCol) {
		b.Coords[faRow][faCol] = '?'
	} else if b.Coords[faRow][faCol] >= 'A' && b.Coords[faRow][faCol] <= 'Z' {
		b.Coords[faRow][faCol] = b.Coords[faRow][faCol] + 'a' - 'A'
	} else if b.Coords[faRow][faCol] >= '0' && b.Coords[faRow][faCol] <= '9' {
		b.Coords[faRow][faCol] = b.Coords[faRow][faCol] - 10
	}

	for true {
		faRow, faCol, err := b.FindFirstAvailableSpotVertical(word)

		if err != nil {
			break
		}

		var faCoords [2]int
		faCoords[0] = faRow
		faCoords[1] = faCol

		ret = append(ret, faCoords)

		/* block the spot so that it isn't considered available for next iteration */
		if b.IsOpenSpot(faRow, faCol) {
			b.Coords[faRow][faCol] = '?'
		} else if b.Coords[faRow][faCol] >= 'A' && b.Coords[faRow][faCol] <= 'Z' {
			b.Coords[faRow][faCol] = b.Coords[faRow][faCol] + 'a' - 'A'
		} else if b.Coords[faRow][faCol] >= '0' && b.Coords[faRow][faCol] <= '9' {
			b.Coords[faRow][faCol] = b.Coords[faRow][faCol] - 10
		}
	}

	/* remove all placeholders */
	for i := 0; i < b.ROWS; i++ {
		for j := 0; j < b.COLS; j++ {
			if b.Coords[i][j] == '?' {
				b.Coords[i][j] = OPEN
			} else if b.Coords[i][j] >= 'a' && b.Coords[i][j] <= 'z' {
				b.Coords[i][j] = b.Coords[i][j] + 'A' - 'a'
			} else if b.Coords[i][j] >= 38 && b.Coords[i][j] <= 47 {
				b.Coords[i][j] = b.Coords[i][j] + 10
			}
		}
	}

	return ret, nil
}

func (b *Board) PlaceWordHorizontal(row int, col int, word string) error {
	if !b.DoesWordFitHorizontal(row, col, word) {
		return errors.New("word " + word + " doesn't fit")
	}

	wordlen := len(word)

	for i := 0; i < wordlen; i++ {
		b.Coords[row][col+i] = word[i]
	}

	return nil
}

func (b *Board) PlaceWordVertical(row int, col int, word string) error {
	if !b.DoesWordFitVertical(row, col, word) {
		return errors.New("word " + word + " doesn't fit")
	}

	wordlen := len(word)

	for i := 0; i < wordlen; i++ {
		b.Coords[row+i][col] = word[i]
	}

	return nil
}

func (b *Board) PlaceWord(placement Placement) error {
	if placement.Direction == HORIZONTAL {
		return b.PlaceWordHorizontal(placement.Coords[0], placement.Coords[1], placement.Word)
	} else if placement.Direction == VERTICAL {
		return b.PlaceWordVertical(placement.Coords[0], placement.Coords[1], placement.Word)
	} else {
		return errors.New("invalid direction provided to PlaceWord()")
	}
}

func (b Board) RemoveSolvedWords(words []string) (newWords []string) {
	newWords = words

	origLen := len(words)

	for i := origLen - 1; i >= 0; i-- {
		curWord := newWords[i]
		if b.IsWordOnBoard(curWord) {
			newWords = RemoveWordFromList(curWord, newWords)
		}
	}

	return newWords
}
